init();
zeta=0:1.2/2048:1.2;
zetaSqrt=sqrt(4.*(zeta.^4)-4.*(zeta.^2)+2);
BW=sqrt(1-2.*zeta.^2+zetaSqrt);
make_plot(zeta,BW,"BW/wn Plot","zeta","BW/wn");
exportgraphics(gcf,'question2Answer.eps','ContentType','vector')
