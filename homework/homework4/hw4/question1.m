init();
zeta=0.1:0.7/2048:0.7;
zetaSqrt=sqrt(1-zeta.^2);
mr=1./(2.*zeta.*zetaSqrt);
make_plot(zeta,mr,"Mr Plot","zeta","Mr");
exportgraphics(gcf,'question1Answer.eps','ContentType','vector')
